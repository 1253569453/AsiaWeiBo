//
//  AppDelegate.swift
//  AsiaWeiBo
//
//  Created by 史亚洲 on 16/1/13.
//  Copyright © 2016年 itcast. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        setThemeColor()
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        window?.rootViewController = MainViewController()
        window?.makeKeyAndVisible()


        return true
    }
    
    //========设置全局的导航控制器的颜色
    func setThemeColor() {
    
    UINavigationBar.appearance().tintColor = UIColor.orangeColor()
        
    
    
    }

   
}

