//
//  MainViewController.swift
//  AsiaWeiBo
//
//  Created by 史亚洲 on 16/1/13.
//  Copyright © 2016年 itcast. All rights reserved.
//

import UIKit


class MainViewController: UITabBarController {
    //注意按钮点击事件的调用是有运行循环监听的,并且以消息机制传递的,因此按钮监听不能单纯设置为private,否则必须添加@objc
    //实现加号按钮点击方法
    @objc private func plusBtnClick() {
    
    print(__FUNCTION__)
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //1.设置自定义tabBar,需要使用KVC赋值,系统的是只读的
        let mainTabBar = MainTabBar()
        setValue(mainTabBar, forKey: "tabBar")
        
        //2.添加加号按钮点击事件
        mainTabBar.plusButton.addTarget(self, action: "plusBtnClick", forControlEvents: .TouchUpInside)

        
     addChildViewControllers()
    }


    
    //2.添加四个子控制器视图
  private  func addChildViewControllers() {
        addChildViewController(HomeTableViewController(), title: "首页", imageName: "tabbar_home")
        addChildViewController(MessageTableViewController(), title: "消息", imageName: "tabbar_message_center")
        addChildViewController(DiscoverTableViewController(), title: "发现", imageName: "tabbar_discover")
        addChildViewController(ProfileTableViewController(), title: "我", imageName: "tabbar_profile")
    }
    
    
    //1.重构代码,抽取参数:控制器,标题,图片
  private  func addChildViewController(vc: UIViewController,title: String,imageName: String) {
        vc.title = title
        vc.tabBarItem.image = UIImage(named: imageName)
        //设置tabBar颜色
        self.tabBar.tintColor = UIColor.orangeColor()
        
        let nav = UINavigationController(rootViewController: vc)
        addChildViewController(nav)
     
        
    }
}
