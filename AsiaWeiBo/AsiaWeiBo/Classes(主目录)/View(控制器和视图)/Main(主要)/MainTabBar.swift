//
//  MainTabBar.swift
//  AsiaWeiBo
//
//  Created by 史亚洲 on 16/1/13.
//  Copyright © 2016年 itcast. All rights reserved.
//

import UIKit

class MainTabBar: UITabBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(plusButton)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //2.设置尺寸大小
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let w = self.bounds.size.width/5
        let h = self.bounds.size.height
        let rect = CGRect(x: 0, y: 0, width: w, height: h)
        var index: CGFloat = 0
        for subView in subviews {
            
            if subView.isKindOfClass(NSClassFromString("UITabBarButton")!) {
            subView.frame = CGRectOffset(rect, index * w, 0)
                index += (index == 1 ? 2 : 1)
  
            }

        }
        
        plusButton.frame = CGRectOffset(rect, w * 2, 0)
        
        
    }
    
    
    //1.懒加载加号按钮
    lazy var plusButton: UIButton = {
    
        //1.1初始化按钮
        let btn = UIButton()
        
        //1.2设置加号图片
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: .Normal)
        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: .Highlighted)
        
        //1.3设置加号按钮背景图片
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: .Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: .Highlighted)
        
        //1.4设置加号按钮大小
        btn.sizeToFit()
    
    
        return btn
    }()

}
