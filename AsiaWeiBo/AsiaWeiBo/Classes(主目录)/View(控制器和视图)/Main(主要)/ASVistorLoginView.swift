//
//  VistorLoginView.swift
//  AsiaWeiBo
//
//  Created by 史亚洲 on 16/1/14.
//  Copyright © 2016年 itcast. All rights reserved.
//
/*

1.设置代理方法
2.设置代理属性
3.设置代理,实现代理方法

*/
import UIKit
import SnapKit


@objc protocol ASVistorLoginViewDelegate: NSObjectProtocol {
    
    //协议方法
    //1.用户将要登录
    func userWillLogin()
    
    //2.用户将要注册
    optional func userWillRegister()
 
    
}
class ASVistorLoginView: UIView {
    //声明协议属性
    weak var vistorLoginViewDelegate: ASVistorLoginViewDelegate?
    
    
//===============按钮的监听方法===============
    //1.登录
    @objc private func loginBtnClick() {
    vistorLoginViewDelegate?.userWillLogin()
    
    }
    //2.注册
    @objc private func registerBtnClick() {
    
    vistorLoginViewDelegate?.userWillRegister?()
    
    }

    
    func setupInfoWith(imageName: String?,tipText: String){
        if let img = imageName {
            circleicon.image = UIImage(named: img)
    
            //隐藏大房子
            largeHouse.hidden = true
            bringSubviewToFront(circleicon)
     
            
        }else {
            
            startAnnimation()
        }
        tipLabel.text = tipText
        
    }
    
    //============3.给主页设置动画效果==========
    func startAnnimation() {
        
        let basic = CABasicAnimation(keyPath: "transform.rotation")
        basic.duration = 10
        basic.repeatCount = MAXFLOAT
        basic.toValue = 2 * M_PI
        basic.removedOnCompletion = false
        circleicon.layer.addAnimation(basic, forKey: nil)
   
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //===========2.设置UI界面==============
    func setupUI() {
        addSubview(circleicon)
        addSubview(backView)
        addSubview(largeHouse)
        addSubview(tipLabel)
        addSubview(loginBtn)
        addSubview(registerBtn)
        
        circleicon.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(self.snp_centerX)
            make.centerY.equalTo(self.snp_centerY).offset(-80)
        }
        
        largeHouse.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(circleicon.snp_center)
        }
        
        tipLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(circleicon.snp_bottom).offset(20)
            make.centerX.equalTo(circleicon.snp_centerX)
            make.width.equalTo(250)
            make.height.equalTo(50)
        }
        
        loginBtn.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(tipLabel.snp_left)
            make.top.equalTo(tipLabel.snp_bottom).offset(20)
            make.width.equalTo(100)
        }
        
        registerBtn.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(tipLabel.snp_right)
            make.top.equalTo(tipLabel.snp_bottom).offset(20)
            make.width.equalTo(100)
        }
        
        backView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top)
            make.left.equalTo(self.snp_left)
            make.right.equalTo(self.snp_right)
            make.bottom.equalTo(registerBtn.snp_bottom)
        }
        
        self.backgroundColor = UIColor(white: 0.93, alpha: 1.0)
        
        
        //添加监听事件
        loginBtn.addTarget(self, action: "loginBtnClick", forControlEvents: .TouchUpInside)
        registerBtn.addTarget(self, action: "registerBtnClick", forControlEvents: .TouchUpInside)
        
    }
    
    
    
    
    //===========1.懒加载所有的视图按钮================
    //1.1圆圈
    lazy var circleicon: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    
    //1.2大房子
    lazy var largeHouse: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    
    //3.提示文案
    lazy var tipLabel: UILabel = {
        let l = UILabel()
        l.text  = "关注一些人，回这里看看有什么惊喜关注一些人，回这里看看有什么惊喜"
        l.font = UIFont.systemFontOfSize(14)
        
        //居中显示
        l.textAlignment = .Center
        
        //换行显示
        l.numberOfLines = 0
        
        l.sizeToFit()
        return l
        
    }()
    
    //4.登录按钮
    lazy var loginBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("登录", forState: .Normal)
        btn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        btn.titleLabel?.font = UIFont.systemFontOfSize(16)
        let image = UIImage(named: "common_button_white_disable")
        
        //拉伸图片,为宽高的一半
        let w = Int(image!.size.width * 0.5)
        let h = Int(image!.size.height * 0.5)
        btn.setBackgroundImage(image?.stretchableImageWithLeftCapWidth(w, topCapHeight: h), forState: .Normal)
        
        
        return btn
    }()
    
    //1.5注册按钮
    lazy var registerBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("注册", forState: .Normal)
        btn.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        btn.titleLabel?.font = UIFont.systemFontOfSize(16)
        let image = UIImage(named: "common_button_white_disable")
        
        
        let w = Int(image!.size.width * 0.5)
        let h = Int(image!.size.height * 0.5)
        btn.setBackgroundImage(image?.stretchableImageWithLeftCapWidth(w, topCapHeight: h), forState: .Normal)
        
        
        return btn
    }()
    
    //1.6背景图
    lazy var backView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    
    
}
