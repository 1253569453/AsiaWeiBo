//
//  UserAccount.swift
//  AsiaWeiBo
//
//  Created by 史亚洲 on 16/1/14.
//  Copyright © 2016年 itcast. All rights reserved.
//

import UIKit

class UserAccount: NSObject,NSCoding {
    /*
    
    返回值字段	字段类型	字段说明
    access_token	string	用于调用access_token，接口获取授权后的access token。
    expires_in	string	access_token的生命周期，单位是秒数,过期时间
    remind_in	string	access_token的生命周期（该参数即将废弃，开发者请使用expires_in）。
    uid	string	当前授权用户的UID。
    */
    
    var access_token: String?
    
    //过期日期,开发者过期时间为5年,普通用户为3天
    var expires_date: NSDate?
    var expires_in: NSTimeInterval = 0 {
        didSet {
        
        
        expires_date = NSDate(timeIntervalSinceNow: expires_in)
        
        }
    
    
    }
    var uid: String?
    var name: String?
    //大图
    var avatar_large: String?
    
    //1.KVC设置赋值
    init(dict:[String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    //2.过滤key值
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }

    //3.重写对象的描述信息
    override var description: String {
    let keys = ["access_token","expires_in","uid","name","avatar_large","expires_date"]
        let dict = self.dictionaryWithValuesForKeys(keys)
        return dict.description
    }
    
    //4.保存自定义对象到沙盒
    func saveAccount() {
    //拼接路径
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("account.plist")
    
    print(path)
        NSKeyedArchiver.archiveRootObject(self, toFile: path)
    
    }
    
    //3.获取磁盘中的归档对象
    class func loadUserAccount() -> UserAccount? {
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("account.plist")
        if let account = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? UserAccount {
        
        return account
        }
        return nil
    }
    
    
    
    //2.归档操作,将自定义对象转化为二进制数据,类似于序列化
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeDouble(expires_in, forKey: "expires_in")
        aCoder.encodeObject(uid, forKey: "uid")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        aCoder.encodeObject(expires_date, forKey: "expires_date")
    }
    
    //1.解归档操作,将二进制数据转化为对象
     required init?(coder aDecoder: NSCoder) {
       access_token = aDecoder.decodeObjectForKey("access_token") as? String
        //基本数据类型可以直接进行解归档操作
        expires_in = aDecoder.decodeDoubleForKey("expires_in")
        uid = aDecoder.decodeObjectForKey("uid") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
        expires_date = aDecoder.decodeObjectForKey("expires_date")as? NSDate
        
    }
    
    
    

}
